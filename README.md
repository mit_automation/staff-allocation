# Staff Allocation

## Name
This Project allows to allocate the staff for the corresponding units. It also have a sheet to collect the staff availability, merge them together and subsequently use the allocation sheet to assign them interactively.

## Installation
There is no installation step as such. Just download and run.

## Usage
#For collecting the staff availability:
1) Update the staff id in the AllStaffs Sheet.
2) Run the Macros in CreateAvailabilitySheet using i) Press Alt+F8, ii) run the macro ConsollidateData. 
3) Share the created sheet with the individual staff and ask them to update.
4) Run the consolidateData again to consolidate them into Consolidated Sheet.

#For Staff Allocation:
1) Copy the Consolidate sheet in the availability Sheet. 
2) Open the Allocation Sheet. Make sure it is populated with all the units already.
3) Run the Macro using i) Press Alt+F8, ii) run the macro Main.
4) Now start selecting the staff from the Drop Down list.
5) Every time you allocate it will update the availability sheet and rerun the macro.
6) You can use clear option to clear the allocation for that unit and staff. 

## Authors
Dr. Samar Shailendra

## License
This is distributed under GNU GPL v3.0. 

## Project status
This is always ongoing with new feature availability.